// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.
import PackageDescription

let package = Package(
  name: "Calc",
  dependencies: [
    .package(
      name: "swift-argument-parser",
      url: "https://github.com/apple/swift-argument-parser",
      .upToNextMinor(from: "0.4.3")
    ),
  ],
  targets: [
    .target(
      name: "Calc",
      dependencies: [
        .product(name: "ArgumentParser", package: "swift-argument-parser"),
      ]
    ),
    .testTarget(
      name: "CalcTests",
      dependencies: ["Calc"]
    )
  ]
)
